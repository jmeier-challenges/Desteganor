package de.jm.view;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

/**
 * User: T05365A
 * Date: 12.11.12
 * Time: 09:43
 */
public class SaveImageAction extends AbstractAction {
	private ImageInterface imageInterface;
	private File selectedFile;
    private File directory;

	@Override
	public void actionPerformed(ActionEvent event) {
		JFileChooser chooser = LoadImageAction.getFileChooser(directory);
		chooser.showOpenDialog(null);
		selectedFile = chooser.getSelectedFile();

		if (selectedFile != null) {
            directory = new File(selectedFile.getParent());
        }

		try {
			ImageIO.write(imageInterface.getBufferedImage(), getFormat(), selectedFile);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private String getFormat() {
		String name = selectedFile.getName();
		String extension = getExtension(name);
		if (extension.isEmpty()) {
			return "PNG";
		} else {
			return extension.toUpperCase();
		}
	}

	private String getExtension(String filename) {
		String[] fileAndExtension = filename.split("\\.");
		if (fileAndExtension.length == 2) {
			return fileAndExtension[1].toUpperCase();
		} else {
			return "";
		}
	}

	public SaveImageAction(ImageInterface imageInterface) {
		super("Save Image");
		this.imageInterface = imageInterface;
	}
}
