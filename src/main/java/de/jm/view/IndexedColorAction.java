package de.jm.view;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * User: T05365A
 * Date: 12.11.12
 * Time: 10:49
 */
public class IndexedColorAction extends AbstractAction {
	private ImageInterface imageInterface;

	public IndexedColorAction(ImageInterface imageInterface) {
		super("Indexed Colors");
		this.imageInterface = imageInterface;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		IndexedColorFrame colorFrame = new IndexedColorFrame();
		colorFrame.setImage(imageInterface.getBufferedImage());
		colorFrame.setVisible(true);
	}
}
