package de.jm.view;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * User: T05365A
 * Date: 12.11.12
 * Time: 09:43
 */
public class LoadImageAction extends AbstractAction {
	private ImageInterface imageInterface;
	private File selectedFile;
	private File directory;

	@Override
	public void actionPerformed(ActionEvent event) {
		JFileChooser chooser = getFileChooser(directory);
		chooser.showOpenDialog(null);
		selectedFile = chooser.getSelectedFile();
		if (selectedFile != null) {
            directory = new File(selectedFile.getParent());
        }
		reload();
	}

	public void reload() {
		try {
			if (selectedFile != null) {

				BufferedImage image = ImageIO.read(selectedFile);

				imageInterface.setBufferdImage(image);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static JFileChooser getFileChooser(File directory) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setFileFilter(new ImageFilter());
		if (directory != null) {
            fileChooser.setCurrentDirectory(directory);
        }
		return fileChooser;
	}

	public LoadImageAction(ImageInterface imageInterface) {
		super("Load Image");
		this.imageInterface = imageInterface;
	}

	public static void main(String[] args) {

		String names[] = ImageIO.getReaderFormatNames();
		for (String name : names) {
			System.out.println(name);
		}
		System.out.println(names.length);

	}
}
