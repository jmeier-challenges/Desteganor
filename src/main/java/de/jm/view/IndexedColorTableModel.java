package de.jm.view;

import de.jm.util.ColorInfo;
import de.jm.util.JMImage;
import de.jm.util.JMImageImpl;

import javax.swing.table.AbstractTableModel;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class IndexedColorTableModel extends AbstractTableModel {
	private List<ColorInfo> rows;
	private String[] columnNames = {"Index", "RGB", "Alpha", "Red", "Green", "Blue", "Count"};

	public ColorInfo getColorModel(int selectedRow) {
		if (isValidRow(selectedRow)) {
			return rows.get(selectedRow);
		} else {
			return new ColorInfo(0, 0, 0);
		}
	}

	private boolean isValidRow(int row) {
		return rows != null && row >= 0 && row < rows.size();
	}

	private boolean isValidCol(int col) {
		return col >= 0 && col < columnNames.length;
	}

    @Override
	public int getRowCount() {
		if (rows == null) {
			return 0;
		} else {
			return rows.size();
		}
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		if (columnIndex == 1) {
			return String.class;
		} else {
			return Integer.class;
		}
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (!isValidRow(rowIndex)) {
			return "";
		}

		ColorInfo colorInfo = rows.get(rowIndex);
		switch (columnIndex) {
            case 0:
                return colorInfo.getIndex();
			case 1:
				return Integer.toHexString(colorInfo.getRgb()).toUpperCase();
			case 2:
				return colorInfo.getAlpha();
			case 3:
				return colorInfo.getRed();
			case 4:
				return colorInfo.getGreen();
			case 5:
				return colorInfo.getBlue();
			case 6:
				return colorInfo.getCount();
			default: return "";
		}
	}

	@Override
	public String getColumnName(int column) {
		if (isValidCol(column)) {
			return columnNames[column];
		} else {
			return "";
		}
	}

    public void setImage(BufferedImage image) {
		rows = new ArrayList<>();

        JMImage jmImage = new JMImageImpl(image);

        rows.addAll(jmImage.getColorInfos());
	}
}
