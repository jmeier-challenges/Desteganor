package de.jm.view;

import de.jm.filter.BitImageFilter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;

/**
 * User: T05365A
 * Date: 12.11.12
 * Time: 09:05
 */
public class Desteganor extends JFrame implements ImageInterface, KeyListener, MouseWheelListener {
	private Action loadImageAction;
	private Action saveImageAction;
	private Action showColorAction;
    private Action showIndexedColorAction;

	private BufferedImage bufferedImage;

	private int scale = 1;
	private ImagePanel panel;
	private JScrollPane scrollPane;

	private BitImageFilter bitImageFilter;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Desteganor ex = new Desteganor();
				ex.setVisible(true);
			}
		});
	}

	public Desteganor() {
		loadImageAction = new LoadImageAction(this);
		saveImageAction = new SaveImageAction(this);
		showColorAction = new UsedColorsAction(this);
        showIndexedColorAction = new IndexedColorAction(this);
		bitImageFilter = new BitImageFilter();

		initializeMenu();

		setTitle("Desteganor");
		setSize(1000, 600);
		setMaximumSize(new Dimension(1200, 1000));
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		addKeyListener(this);
	}

	private void initializeMenu() {
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("File");
		menuBar.add(menu);
		menu.add(new JMenuItem(loadImageAction));
		menu.add(new JMenuItem(saveImageAction));

		menu = new JMenu("Tools");
		menuBar.add(menu);
		menu.add(new JMenuItem(showColorAction));
        menu.add(new JMenuItem(showIndexedColorAction));
		bitImageFilter = new BitImageFilter();
		bitImageFilter.setRedMask(1);
		menu.add(new JMenuItem(new AbstractAction("Bitmask Filter") {
			@Override
			public void actionPerformed(ActionEvent e) {
				doFilter(bitImageFilter);
			}
		}));

		setJMenuBar(menuBar);
	}

	private void doFilter(BufferedImageOp filter) {
		EventQueue.invokeLater(new FilterRunnable(filter));
	}

	class FilterRunnable implements Runnable {
		BufferedImageOp filter;

		FilterRunnable(BufferedImageOp filter) {
			this.filter = filter;
		}

		@Override
		public void run() {
			setBufferdImage(filter.filter(bufferedImage, null));
		}
	}

	@Override
	public BufferedImage getBufferedImage() {
		return bufferedImage;
	}

	@Override
	public void setBufferdImage(BufferedImage image) {
		bufferedImage = image;
		showImage(image);
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyChar() == '+') {
			scaleUp();
		} else if (e.getKeyChar() == '-') {
			scaleDown();
		}
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	private void scaleUp() {
		scale++;
		showImage(scale(scale, scale, bufferedImage));
	}

	private void scaleDown() {
		if (scale > 0) {
			scale--;
		}

		if (scale == 0) {
			showImage(bufferedImage);
		} else {
			showImage(scale(scale, scale, bufferedImage));
		}
	}

	private BufferedImage scale(float scaleX, float scaleY, BufferedImage bufferedImage) {
		AffineTransform tx = new AffineTransform();
		tx.scale(scaleX, scaleY);

		AffineTransformOp op = new AffineTransformOp(tx,
			AffineTransformOp.TYPE_BILINEAR);
		return  op.filter(bufferedImage, null);
	}


	private void showImage(final BufferedImage image) {
		if (panel == null) {
			panel = new ImagePanel();
			panel.setMaximumSize(new Dimension(1000, 800));
			scrollPane = new JScrollPane(panel);
			scrollPane.addMouseWheelListener(this);
			getContentPane().add(scrollPane);
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int width = image.getWidth();
				int height = image.getHeight();
				panel.setImage(image);
				panel.setPreferredSize(new Dimension(width, height));
				if (width <= panel.getMaximumSize().width && height <= panel.getMaximumSize().height) {
					pack();
				}
				validate();
			}
		});
	}

	class ImagePanel extends JPanel {
		BufferedImage image;

		public void setImage(BufferedImage image) {
			this.image = image;
		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawImage(image, 0, 0, null);
		}
	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		int x = e.getX();
		int y = e.getY();

		int notches = e.getWheelRotation();
		if (notches < 0) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					scaleUp();
					//scrollPane.getViewport().setViewPosition(new Point(100,100));
				}
			});
		} else {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					scaleDown();
					//scrollPane.getViewport().setViewPosition(new Point(100,100));
				}
			});
		}
	}
}
