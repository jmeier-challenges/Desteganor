package de.jm.view;

import de.jm.filter.ColorFilter;
import de.jm.util.ColorInfo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

/**
 * User: T05365A
 * Date: 12.11.12
 * Time: 12:00
 */
public class ColorFrame extends JFrame {
	private JScrollPane scrollPane;
	private BufferedImage image;
	private ColorFilter filter;
	private ColorTableModel tableModel;
	private JTable colorTable;
	private JPopupMenu popupMenu;


	public ColorFrame() throws HeadlessException {
		setTitle("Used Colors");
		setSize(500, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		filter = new ColorFilter();

		initializeMenu();
	}

	private void initializeMenu() {

		popupMenu = new JPopupMenu();

		popupMenu.add(new JMenuItem(new AbstractAction(filter.toString()) {
			@Override
			public void actionPerformed(ActionEvent e) {
				doFilter();
			}
		}));

	}

	private void doFilter() {
		filter.setColors(getSelectedColors());
		BufferedImage filteredImage = filter.filter(image, null);
		Desteganor desteganor = new Desteganor();
		desteganor.setTitle("Color Filter: " + toString(filter.getColors()));
		desteganor.setBufferdImage(filteredImage);
		desteganor.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		desteganor.setVisible(true);
	}

	public static String toString(int[] colors) {
		StringBuilder builder = new StringBuilder();
		for (int color : colors) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append(Integer.toHexString(color));

		}

		return builder.toString();
	}

	public void setImage(BufferedImage image) {
		this.image = image;
		if (scrollPane != null) {
			getContentPane().remove(scrollPane);
		}

		tableModel = new ColorTableModel();
		tableModel.setImage(image);
		colorTable = new JTable(tableModel);
		colorTable.setAutoCreateRowSorter(true);
		colorTable.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					doFilter();
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showPopupMenu(e);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showPopupMenu(e);
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});
		scrollPane = new JScrollPane(colorTable);
		getContentPane().add(scrollPane);
	}

	private void showPopupMenu(MouseEvent e) {
		popupMenu.show(e.getComponent(), e.getX(), e.getY());
	}


	public int[] getSelectedColors() {
		int [] selectedRows = colorTable.getSelectedRows();
		int[] colors = new int[selectedRows.length];
		for (int i = 0; i < selectedRows.length; i++) {
			int selectedRow  = selectedRows[i];
			ColorInfo colorInfo = tableModel.getColorModel(colorTable.convertRowIndexToModel(selectedRow));
			colors[i] = colorInfo.getRgb();
		}
		return colors;
	}
}
