package de.jm.view;

import de.jm.filter.IndexedColorFilter;
import de.jm.util.ColorInfo;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

public class IndexedColorFrame extends JFrame {
	private JScrollPane scrollPane;
	private BufferedImage image;
	private IndexedColorFilter filter;
	private IndexedColorTableModel tableModel;
	private JTable colorTable;
	private JPopupMenu popupMenu;


	IndexedColorFrame() throws HeadlessException {
		setTitle("Indexed Colors");
		setSize(500, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		filter = new IndexedColorFilter();

		initializeMenu();
	}

	private void initializeMenu() {

		popupMenu = new JPopupMenu();

		popupMenu.add(new JMenuItem(new AbstractAction(filter.toString()) {
			@Override
			public void actionPerformed(ActionEvent e) {
			    filter.setIndexFilter();
				doFilter();
			}
		}));
        popupMenu.add(new JMenuItem(new AbstractAction("Prime filter (first pixel has index 1)") {
            @Override
            public void actionPerformed(ActionEvent e) {
                filter.setPrimeFilter(true);
                doFilter();
            }
        }));
        popupMenu.add(new JMenuItem(new AbstractAction("Prime filter (first pixel has index 0)") {
            @Override
            public void actionPerformed(ActionEvent e) {
                filter.setPrimeFilter(false);
                doFilter();
            }
        }));

	}

	private void doFilter() {
		filter.setIndexe(getSelectedIndexe());
		BufferedImage filteredImage = filter.filter(image, null);
		Desteganor desteganor = new Desteganor();
		desteganor.setTitle("Index Color Filter: " + toString(filter.getIndexe()));
		desteganor.setBufferdImage(filteredImage);
		desteganor.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		desteganor.setVisible(true);
	}

	private static String toString(int[] filterColors) {
		StringBuilder builder = new StringBuilder();
		for (int color : filterColors) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append(Integer.toHexString(color));
		}

		return builder.toString();
	}

	public void setImage(BufferedImage image) {
		this.image = image;
		if (scrollPane != null) {
			getContentPane().remove(scrollPane);
		}

		tableModel = new IndexedColorTableModel();
		tableModel.setImage(image);
		colorTable = new JTable(tableModel);
		colorTable.setAutoCreateRowSorter(true);
		colorTable.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();
					filter.setIndexFilter();
					doFilter();
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showPopupMenu(e);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showPopupMenu(e);
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});
		scrollPane = new JScrollPane(colorTable);
		getContentPane().add(scrollPane);
	}

	private void showPopupMenu(MouseEvent e) {
		popupMenu.show(e.getComponent(), e.getX(), e.getY());
	}


	private int[] getSelectedIndexe() {
		int [] selectedRows = colorTable.getSelectedRows();
		int[] indexe = new int[selectedRows.length];
		for (int i = 0; i < selectedRows.length; i++) {
			int selectedRow  = selectedRows[i];
			ColorInfo colorInfo = tableModel.getColorModel(colorTable.convertRowIndexToModel(selectedRow));
			indexe[i] = colorInfo.getIndex();
		}
		return indexe;
	}
}
