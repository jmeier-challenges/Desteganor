package de.jm.view;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Created with IntliJ IDEA.
 * User: T05365A
 * Date: 12.11.12
 * Time: 09:41
 */
public class ImageFilter extends FileFilter {
	final static private String imageExtension = "jpg jpeg png gif bmp tiff tif";
	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}

		return isImage(f);
	}

	private boolean isImage(File f) {
		String[] fileAndExtension = f.getName().split("\\.");
		if (fileAndExtension.length == 2) {
			return imageExtension.indexOf(fileAndExtension[1].toLowerCase()) >= 0;
		} else  {
			return false;
		}
	}

	@Override
	public String getDescription() {
		return "Image Filter";
	}
}
