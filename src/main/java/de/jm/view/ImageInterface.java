package de.jm.view;

import java.awt.image.BufferedImage;

/**
 * User: T05365A
 * Date: 12.11.12
 * Time: 09:57
 */
public interface ImageInterface {

	BufferedImage getBufferedImage();
	void setBufferdImage(BufferedImage image);
}
