package de.jm.view;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * User: T05365A
 * Date: 12.11.12
 * Time: 10:49
 */
public class UsedColorsAction extends AbstractAction {
	private ImageInterface imageInterface;

	public UsedColorsAction(ImageInterface imageInterface) {
		super("Used Colors");
		this.imageInterface = imageInterface;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ColorFrame colorFrame = new ColorFrame();
		colorFrame.setImage(imageInterface.getBufferedImage());
		colorFrame.setVisible(true);
	}
}
