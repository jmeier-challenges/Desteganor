package de.jm.filter;

import java.awt.image.BufferedImage;
import java.awt.image.DirectColorModel;

/**
 * User: T05365A
 * Date: 13.11.12
 * Time: 08:26
 */
public class ColorFilter extends AbstractFilter {
	private int[] colors;

	@Override
	public BufferedImage filter(BufferedImage src, BufferedImage dest) {
		if (dest == null) {
			DirectColorModel directCM = new DirectColorModel(32,
				0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
			dest = createCompatibleDestImage(src, directCM);
		}

		int width = src.getWidth();
		int height = src.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int rgb = src.getRGB(x, y);
				if (isFiltered(rgb)) {
					rgb = 0xff000000;
				} else {
					rgb = 0xffffffff;
				}
				dest.setRGB(x, y, rgb);
			}
		}

		return dest;
	}

	private boolean isFiltered(int rgb) {
		for (int color : colors) {
			if (rgb == color) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "Color Filter";
	}

	public int[] getColors() {
		return colors;
	}

	public void setColors(int[] colors) {
		this.colors = colors;
	}

	public void setColor(int color) {
		colors = new int[1];
		colors[0] = color;
	}
}
