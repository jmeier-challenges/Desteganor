package de.jm.filter;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;

/**
 * User: T05365A
 * Date: 13.11.12
 * Time: 13:41
 */
public class BitImageFilter extends AbstractFilter {
	private int blueMask;
	private int redMask;
	private int greenMask;
	private ColorModel colorModel;

	public BitImageFilter() {
		setRedMask(0);
		setGreenMask(0);
		setBlueMask(0);
		colorModel = ColorModel.getRGBdefault();
	}

	public int getBlueMask() {
		return blueMask;
	}

	public void setBlueMask(int blueMask) {
		this.blueMask = blueMask;
	}

	public int getRedMask() {
		return redMask;
	}

	public void setRedMask(int redMask) {
		this.redMask = redMask;
	}

	public int getGreenMask() {
		return greenMask;
	}

	public void setGreenMask(int greenMask) {
		this.greenMask = greenMask;
	}

	@Override
	public BufferedImage filter(BufferedImage src, BufferedImage dest) {
		if (dest == null) {
			DirectColorModel directCM = new DirectColorModel(32,
				0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
			dest = createCompatibleDestImage(src, directCM);
		}

		int width = src.getWidth();
		int height = src.getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int rgb = src.getRGB(x, y);
				if (isFiltered(rgb)) {
					rgb = 0xff000000;
				} else {
					rgb = 0xffffffff;
				}
				dest.setRGB(x, y, rgb);
			}
		}

		return dest;
	}

	private boolean isFiltered(int rgb) {
		return isFilteredRed(rgb) && isFilteredGreen(rgb) && isFilteredBlue(rgb);
	}

	private boolean isFilteredBlue(int rgb) {
		if (getBlueMask() > 0) {
			int color = colorModel.getBlue(rgb);
			return (color & getBlueMask()) == 1;
		} else {
			return true;
		}
	}
	private boolean isFilteredRed(int rgb) {
		if (getRedMask() > 0) {
			int color = colorModel.getRed(rgb);
			return (color & getRedMask()) == 1;
		} else {
			return true;
		}
	}
	private boolean isFilteredGreen(int rgb) {
		if (getGreenMask() > 0) {
			int color = colorModel.getGreen(rgb);
			return (color & getGreenMask()) == 1;
		} else {
			return true;
		}
	}

	@Override
	public String toString() {
		return "Bitmask Filter";
	}
}
