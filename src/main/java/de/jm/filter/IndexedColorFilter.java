package de.jm.filter;

import de.jm.util.JMImageImpl;

import java.awt.image.BufferedImage;
import java.awt.image.DirectColorModel;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * User: T05365A
 * Date: 13.11.12
 * Time: 08:26
 */
public class IndexedColorFilter extends AbstractFilter {
	private int[] indexe;
    private Predicate<Integer> sfilter = this::isFiltered;
    private boolean zeroIndexIsOne = true;

	@Override
	public BufferedImage filter(BufferedImage src, BufferedImage dest) {
		if (dest == null) {
			DirectColorModel directCM = new DirectColorModel(32,
				0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000);
			dest = createCompatibleDestImage(src, directCM);
		}

        int width = src.getWidth();

        JMImageImpl jmImage = new JMImageImpl(src);

		List<Integer> data = jmImage.getIndexedPixels();
        for (int i = 0; i < data.size(); i++) {
            int index = data.get(i);
            int y = i / width;
            int x = i % width;

            if (sfilter.test(index)) {
                dest.setRGB(x, y, 0xff000000);
            } else {
                dest.setRGB(x, y, 0xffffffff);
            }
        }
		return dest;
	}

	private static List<Integer> primesList = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59,
            61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179,
            181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251);

	private boolean isPrimeFiltered(int index) {
	    if (zeroIndexIsOne) {
            return primesList.contains(index+1);
        } else {
            return primesList.contains(index);
        }
    }

	private boolean isFiltered(int index) {
		for (int i : indexe) {
			if (i == index) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "Indexed Color Filter";
	}

	public int[] getIndexe() {
		return indexe;
	}

	public void setIndexe(int[] indexe) {
		this.indexe = indexe;
	}

	public void setColor(int index) {
		indexe = new int[1];
		indexe[0] = index;
	}

	public void setPrimeFilter(boolean zeroIndexIsOne) {
	    this.zeroIndexIsOne = zeroIndexIsOne;
        sfilter = this::isPrimeFiltered;
    }

    public void setPrimeFilter() {
	    setPrimeFilter(true);
    }

    public void setIndexFilter() {
	    sfilter = this::isFiltered;
    }
}
