package de.jm.util;

import java.awt.image.ColorModel;

public class ColorInfo {

    private int index;
    private int rgbColor;
    private int count;

    public ColorInfo(int theIndex, int color, int count) {
        index = theIndex;
        rgbColor = color;
        this.count = count;
    }

    public ColorInfo(Integer rgb, Integer count) {
        rgbColor = rgb;
        this.count = count;
    }

    public int getRgb() {
        return rgbColor;
    }

    public int getCount() {
        return count;
    }

    public int getRed() {
        return ColorModel.getRGBdefault().getRed(rgbColor);
    }

    public int getGreen() {
        return ColorModel.getRGBdefault().getGreen(rgbColor);
    }

    public int getBlue() {
        return ColorModel.getRGBdefault().getBlue(rgbColor);
    }

    public int getAlpha() {
        return ColorModel.getRGBdefault().getAlpha(rgbColor);
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {

        return "ColorInfo{" +
                "index=" + index +
                ", rgbColor=" + rgbColor +
                ", count=" + count +
                '}';
    }
}
