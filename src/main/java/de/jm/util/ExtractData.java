package de.jm.util;

public class ExtractData {
    public static final int PLANE0 = 1;
    public static final int PLANE1 = 2;
    public static final int PLANE2 = 4;
    public static final int PLANE3 = 8;
    public static final int PLANE4 = 16;
    public static final int PLANE5 = 32;
    public static final int PLANE6 = 64;
    public static final int PLANE7 = 128;

    public static char extractRedBit(int argb, int bitMask) {
        return extractBit(getRedPart(argb), bitMask);
    }

    public static char extractGreenBit(int argb, int bitMask) {
        return extractBit(getGreenPart(argb), bitMask);
    }

    public static char extractBlueBit(int argb, int bitMask) {
        return extractBit(getBluePart(argb), bitMask);
    }

    public static char extractAlphaBit(int argb, int bitMask) {
        return extractBit(getAlphaPart(argb), bitMask);
    }

    public static int getRedPart(int argb) {
        return (argb & 0xff0000) >> 16;
    }

    public static int getGreenPart(int argb) {
        return (argb & 0xff00) >> 8;
    }

    public static int getBluePart(int argb) {
        return (argb & 0xff);
    }

    public static int getAlphaPart(int argb) {
        return (argb & 0xff000000) >> 24;
    }

    private static char extractBit(int colorPart, int bitMask) {
        return (colorPart & bitMask) == 0 ? '0' : '1';
    }

}
