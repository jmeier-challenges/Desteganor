package de.jm.util;

import java.util.List;

public interface JMImage {
    boolean isIndexedImage();
    List<Integer> getRgbPixels();
    List<Integer> getIndexedPixels();
    List<Integer> getIndexedColors();
    List<ColorInfo> getColorInfos();
    int getWidth();
    int getHeight();
}
