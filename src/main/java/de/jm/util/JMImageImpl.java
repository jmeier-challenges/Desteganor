package de.jm.util;

import javax.imageio.ImageIO;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JMImageImpl implements JMImage {
    private BufferedImage image;

    public JMImageImpl(String imagePath) {
        File imageFile = new File(imagePath);
        try {
            image = ImageIO.read(imageFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public JMImageImpl(BufferedImage image) {
        this.image = image;
    }

    @Override
    public boolean isIndexedImage() {
        return image.getColorModel() instanceof IndexColorModel;
    }

    @Override
    public List<Integer> getRgbPixels() {
        List<Integer> pixels = new ArrayList<>();

        int width = getWidth();
        int height = getHeight();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                pixels.add(image.getRGB(x, y));
            }
        }

        return pixels;
    }

    @Override
    public List<Integer> getIndexedPixels() {
        List<Integer> pixels = new ArrayList<>();

        if (isIndexedImage()) {
            for (byte color : getIndexedImageData()) {
                pixels.add(color & 0xff);
            }
        }

        return pixels;
    }

    @Override
    public List<Integer> getIndexedColors() {
        List<Integer> pixels = new ArrayList<>();

        if (!isIndexedImage()) {
            return pixels;
        }

        IndexColorModel indexColorModel = (IndexColorModel) image.getColorModel();

        int[] rgbIndexColors = new int[256];
        indexColorModel.getRGBs(rgbIndexColors);

        for (int color : rgbIndexColors) {
            pixels.add(color);
        }
        return pixels;
    }

    @Override
    public List<ColorInfo> getColorInfos() {
        if (isIndexedImage()) {
            return getIndexedColorInfo();
        } else {
            return getRgbColorInfo();
        }
    }

    private List<ColorInfo> getRgbColorInfo() {
        List<ColorInfo> colorInfos = new ArrayList<>();
        Map<Integer, Integer> colors = new HashMap<>();

        int width = getWidth();
        int height = getHeight();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int rgb = image.getRGB(x, y);

                int count = 1;

                if (colors.containsKey(rgb)) {
                    count = colors.get(rgb);
                    count++;
                }
                colors.put(rgb, count);
            }
        }

        for (Integer rgb : colors.keySet()) {
            colorInfos.add(new ColorInfo(rgb, colors.get(rgb)));
        }

        return colorInfos;
    }

    private List<ColorInfo> getIndexedColorInfo() {
        List<ColorInfo> colorInfos = new ArrayList<>();
        ColorModel colorModel = image.getColorModel();

        if (!isIndexedImage()) {
            return colorInfos;
        }

        IndexColorModel indexColorModel = (IndexColorModel) colorModel;

        int[] rgbIndexColors = new int[256];
        int[] counts = getIndexedCounts();
        indexColorModel.getRGBs(rgbIndexColors);

        for (int i = 0; i < rgbIndexColors.length; i++) {
            int rgb = rgbIndexColors[i];
            int count = counts[i];

            ColorInfo colorInfo = new ColorInfo(i, rgb, count);
            colorInfos.add(colorInfo);
        }
        return colorInfos;
    }

    private int[] getIndexedCounts() {
        int[] countColorIndex = new int[256];
        byte[] bytes = getIndexedImageData();
        for (byte b : bytes) {
            int index = (b & 0xff);
            countColorIndex[index]++;
        }
        return  countColorIndex;
    }

    private byte[] getIndexedImageData() {
        WritableRaster raster =  image.getRaster();
        return ((DataBufferByte) raster.getDataBuffer()).getData();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }
}
